define (require)->

  config = require '../config'
    
  class TripModel extends Backbone.Model
    url: =>
      "#{config.restPath}/api/2/issue"

    initialize: (options) ->
      @errors = []
      @bind 'error', (model, errs) =>
        console.log 'TripModel Error', arguments
        @errors = errs 
        errs

    parse: (d) ->
      if d.fields
        _.extend d.fields,
          id: d.id
          key: d.key
      else
        d

    validate:
      summary:
        required: true
      description:
        required: true
      origin:
        required: true
      destination:
        required: true
      departDate:
        required: true
      returnDate:
        required: true
      purchaseAmt:
        required: true
      currency:
        required: true
      assignee:
        required: true
