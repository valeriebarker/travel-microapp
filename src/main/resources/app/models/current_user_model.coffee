define (require)->

  config    = require '../config'
  UserModel = require './user_model'
    
  class CurrentUserModel extends Backbone.Model
    url: "#{config.restPath}/auth/1/session"

    initialize: ->
      @fetch()

    fetch: ->
      super.done =>
        new UserModel
          id        : @get 'name'
          callback  : (d) =>
            @set 'profile', d
            @setLoggedIn()

    setLoggedIn: ->
      $('.curruser').html @get('profile').displayName
      $('.avatar').attr('src', @get('profile').avatarUrls["48x48"])