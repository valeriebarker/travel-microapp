Atlassian Travel Approval Plugin for JIRA
=========================================

    http://<jira_host>/<context_path>/travel

This is a simple travel approval microapp built on top of JIRA 5. It utilizes
JIRA's configurable workflow to allow a custom approval workflow suited
for travel approvals.

![Screenshot](http://f.cl.ly/items/1o0s310i2c2R1v1S452M/Screen%20Shot%202012-06-04%20at%208.45.04%20PM.png)

Travel requests and [flight comparison](http://www.travelers-compare.com/) are mapped to JIRA issues using a custom issue type called
"Travel Request." In addition, this app requires five custom fields to be
created to store data required by the travel approval process.

For now all the custom field names and project id is hard-coded in [`src/main/resources/app/config.coffee`](https://bitbucket.org/atlassian/jira-travel-microapp/src/master/src/main/resources/app/config.coffee).
In the future, we will utilize Jonathan Doklovic's workflow and project
importer in order to bootstrap the creation of the required project, custom
fields, issue type and workflow.


How is this built?
------------------

This app is built primarily as a client-side app that utilizes JIRA 5's 
REST API. The client uses [Backbonejs](http://backbonejs.org), 
[Bootstrap](http://twitter.github.com/bootstrap/), and jQuery. We then packaged 
up the app as a plugin in order to simplify authentication and hosting. The app
is easily accessible through http://<jira>/travel.

How do I hack on this?
----------------------

Check out the CoffeeScript code under:

    src/main/resources/app

Read the [`config.coffee`](https://bitbucket.org/atlassian/jira-travel-microapp/src/master/src/main/resources/app/config.coffee) to learn about how to set up JIRA for this to work. This app is a single page app that uses [Backbonejs](http://backbonejs.org/), 
[CoffeeScript](http://coffeescript.org/), and [Requirejs](http://requirejs.org/).

If you make any changes to the `*.coffee` files, make sure you run the `./build.sh` file 
to recompile them back to `*.js`. This build script will also create a minified version for you 
(`main.min.js`) which is not enabled in the HTML by default. You can also use [LiveReload](http://livereload.com/) for compiling the CoffeeScript on demand. I highly recommend it!

Wanna build on top of this?
---------------------------

Go ahead and fork it. If it's useful to you, let us know 
([@atlassiandev](http://twitter.com/atlassiandev) and [@rmanalan](http://twitter.com/rmanalan)). 
If you have a cool new feature you want to share, send us a pull request.